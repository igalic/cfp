# Puppet Camps

There are two Puppet Camps in June (07 and 09) that are still accepting calls for papers. A low-key session on Voxpupuli would probably be warmly welcomed. Further, it's good practice for the contributor summit / puppetconf itself.

# Bio

Igor is a freelancer and a seasoned Open Source contributor, who usually works on the ops-side of the dev-ops spectrum. With only three years puppet experience, they're *practically* a new-comer.
