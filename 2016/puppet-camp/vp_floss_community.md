# title

## Voxpupuli - an Open Source Speedtrain

# Proposed Session Description (max 125 words)

Vox Pupuli, the Voice of the Puppets, is an Open Source Community centred around Puppet and its ecosystem. The most important idea is to have more than one maintainer. The most powerful effect from that has been a flourishing community moving forward at a pace that our colleagues in the Puppetlabs Module team envy.

In this session I'd like to talk about how we've accomplished that, how *you* can *participate* in and *learn* from our practices.

# Your Session Audience Level

Intermediate

# What do you want the audience to gain from your session?    

After this talk, the audience should know what Voxpupuli is, and why *they* should want to contribute to this open source effort. They will be familiar with open source best practices, many of which can be applied in the enterprise.
