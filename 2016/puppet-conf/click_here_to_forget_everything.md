# Click here to forget everything

## Proposed Session Description (max 125 words)

The life of a consultant is as glorious as the life of a speaker: Travel, airport (security), hotel breakfasts, and lots of repetition.

Every gig brings things to re-learn and re-tell.
Basics, like navigating public transport, learning new colleagues' names, and finding your way around confusingly laid out buildings.
Where it gets interesting for us is relearning new infrastructures, finding your way through a maze of "best practices", encoded in forests of git repositories. Once we've figured out how all of this affects said infrastructure, we can finally make an impact!…

Depending on your day, this can be profoundly humbling, or extremely frustrating. I'd like to provide guide-lines and tools you can build on, to navigate around the big reset button. To foster valuable experiences.

## Your Session Audience Level

Intermediate

## What do you want the audience to gain from your session?    

so many problems are already solved, what persists is our insistence that they are not because we don't know better. 
Resetting your point of view every couple of months presents a unique learning opportunity, and I'd like to encourage more people to use this as practice - even if they are not gig-hopping consultants.
