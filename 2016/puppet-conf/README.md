# PuppetConf 2016

San Diego, 17-21 Oct 2016

Schedule: http://2016.puppetconf.com/schedule/

## Bio

Igor is a freelancer and a seasoned Open Source contributor, who usually works on the ops-side of the dev-ops spectrum. With only three years puppet experience, they're *practically* a new-comer.
