# title

## Keeping Up

# Proposed Session Description (max 125 words)

Puppet keeps moving forward with new features and bug fixes. The language keeps evolving, deprecating old, crusty features, for new, shiny ones.

At the same time modules, the essential building blocks of our infrastructures, move at an even more ludicrous speed.

So how do we keep up? And, more importantly, how do we keep up without burning out?
I'll show some (low) thech methods for how I'm keeping up, and I'd like to invite members of the audience to share their set-backs as well as their success stories.

# Your Session Audience Level

Intermeddiate

# What do you want the audience to gain from your session?    

The audience should gain a confidence to run the latest versions of puppet, to run current versions of modules and participate in improving them. I'd also love to see an exchange in the audience for how to do some of the specifics.
