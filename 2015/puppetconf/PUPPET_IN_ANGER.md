# title

## PUPPET IN ANGER

# Proposed Session Description (max 125 words)

Recently a friend asked me on to write her up a basic howto for getting started with Puppet.
I promised to deliver it over the weekend. A WEEK LATER I WAS STILL PREOCCUPIED WITH YAK SHAVING.

I've always found the Puppetlabs documentation to be good, but not very beginner friendly.
A "beginner" in this context is NOT new to Systems Administration, or even Automation, or Configuration Management. JUST NEW TO PUPPET.

***PUPPET IN ANGER*** IS A HOW-TO FOR PEOPLE WHO WANT - OR *HAVE TO* - GET STARTED WITH PUPPET AND NEED A RELIABLE GUIDE-BOOK.
IT'S ALSO A REFERENCE FOR THOSE WHO GOT LOST DEBUGGING.

# What do you want the audience to gain from your session?

I want my audience to gain the confidence to dig into Puppet.
To started digging into an existing code-base.
To go fearless into debugging.
To get started with their new infrastructure with Puppet.

# Your Session Audience Level

I'm not sure about this one yet.
is there a Beginner, but not *complete* Beginner category?

