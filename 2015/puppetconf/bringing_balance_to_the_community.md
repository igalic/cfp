# title

## Bringing Balance to The Community

# Proposed Session Description (max 125 words)

last year Daniele Sluijters and i founded "Puppet-Community" to create a counter-weight to the Puppetlabs corporation within the Puppet community.

Puppet-Community tries to do two things:

* it's a place where you can bring your modules to a wider community of authors and supporters.

* and, equally importantly a place where we can prototype radical changes, that would be impossible in Puppetlabs, because their modules always need to be compatible with the oldest released and supported puppet versions.

I'd like to reflect on this past year. What technical and personal challenges we faced, and how we've solved them. How we ensured that Puppet-Community doesn't just become a dumping ground for dead projects, but fertile ground instead where new ideas and projects can be grown from.


# Your Session Audience Level

Intermeddiate

# What do you want the audience to gain from your session?    

I'd like those members of the audience who still don't know Puppet-Community to get to know it, and I'd like for the ones who already know it to get a burning sensation to help out ;)

