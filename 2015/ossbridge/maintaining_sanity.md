maintaining sanity
==================

maintaining dozens of foss projects, without burning out
--------------------------------------------------------

i always like to submit challening topics to conferences. topics that force to
pry myself out of my own shell, or explore (my) technical limitations.

last year, i talked at apachecon for the first time (publicly) about my mental
health issues. i talked about depression, addiction, and burn-out.

this year, i'd like to do something more challenging: i'd like to explore how
to maintain numerous open source projects, without burning out. on work, or,
worse yet, on the community aspect itself.
